/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emedia_project;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author heraklesik94
 */
public class RSA {
  
   private BigInteger messageBig;
   private BigInteger encryptBig; 
   private BigInteger privateKey;
   private BigInteger publicKey;
   private BigInteger modulus;
   private final BigInteger one = new BigInteger("1");
   private final SecureRandom random = new SecureRandom();
   
    RSA(int N) {
      BigInteger p = BigInteger.probablePrime(N/2, random);
      BigInteger q = BigInteger.probablePrime(N/2, random);
      BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));

      modulus    = p.multiply(q);                                  
      publicKey  = new BigInteger("65537");     //2^16 + 1
      privateKey = publicKey.modInverse(phi);
   }

   byte[] encryptBytes(byte[] message) {
        messageBig = new BigInteger(1, message);
        encryptBig = encryptBig(messageBig);     
      return encryptBig.toByteArray();
   }

   byte[] decryptBytes(byte[] encrypted) {
       messageBig = new BigInteger(1, encrypted);
       encryptBig = decryptBig(messageBig);     
      return encryptBig.toByteArray();
   }

   BigInteger encryptBig(BigInteger message) {
      return message.modPow(publicKey, modulus);
   }

   BigInteger decryptBig(BigInteger encrypted) {
      return encrypted.modPow(privateKey, modulus);
   }
   
   public String toString() {
      String s = "";
      s += "public  = " + publicKey  + "\n";
      s += "private = " + privateKey + "\n";
      s += "modulus = " + modulus;
      return s;
   }
    
}
