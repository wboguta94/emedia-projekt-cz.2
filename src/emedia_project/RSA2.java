/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emedia_project;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.Cipher;

/**
 *
 * @author herak
 */
public class RSA2 {
    
    private KeyPair keyPair;
    private PublicKey pubKey;
    private PrivateKey privateKey;
    
    RSA2(int keySize) throws NoSuchAlgorithmException{
        // generate public and private keys
        keyPair = buildKeyPair(keySize);
        pubKey = keyPair.getPublic();
        privateKey = keyPair.getPrivate();
    }
    
    public KeyPair buildKeyPair(int keySize) throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);      
        return keyPairGenerator.genKeyPair();
    }

    public byte[] encrypt(byte[] message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);  
        return cipher.doFinal(message);  
    }
    
    public byte[] decrypt(byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        return cipher.doFinal(encrypted);
    }
}
